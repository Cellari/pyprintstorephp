import urllib.request
import json
import os
import re
from collections import OrderedDict
from io import StringIO

##Modifiers for downloading and formatting the results
#url to read
URL = "http://danger.rulez.sk/projects/bruteforceblocker/blist.php"
#replace characters with their values
REPLACE = {'\n' : '', '# ': ''}
#splits downloaded lines into an array at these
SPLIT_TO_ARRAY_AT = '\t'
#field name by which to sort lines
SORT_VALUE = "last reported"

##Storage options
#search for a pattern in the downloaded lines and use it as a file name
REGEX = "[\d\-]+"
#use a prefix for file name
FILE_PREFIX = "data-"
#prefix where to store values
PATH = "./files/"
#mode to use when opening a file
FILE_ACCESS_MODE = 'a'

#downloads and returns results in a list of JSONObjects
def download(url):
    print("Downloading {}".format(url))
    RESPONSES = urllib.request.urlopen(url)
    IO = StringIO()
    KEYS = []
    JSONOBJECTS = []

    index = 0
    for response in RESPONSES:
        #Remove unnecessary characters from the response
        line = response.decode('ascii')
        for k,v in REPLACE.items():
            line = line.replace(k, v)
        
        #convert string to an array
        line = line.split(SPLIT_TO_ARRAY_AT)
        
        #convert values to key value pairs
        array = []
        for f in filter(None, line):
            if index == 0:
                KEYS.append(f.lower())
            else:
                array.append(f)

        if array != []:
            #create dictionary with SORT_VALUE as the first item
            d = OrderedDict(zip(KEYS, array))
            d.move_to_end(SORT_VALUE, last=False) #normal dict is unordered and can result in a changing output

            #convert dictionary to JSON object
            jsonObject = json.dumps(d,IO)
            JSONOBJECTS.append(jsonObject)
        else:
            print("The downloaded keys are: {}".format(KEYS))

        index += 1

    return JSONOBJECTS

def storeValues(fileName, lines, modes):
    if not os.path.exists(PATH):
        print("Creating path: {}".format(PATH))
        os.makedirs(PATH)
        
    #write the lines to a file
    fullPath = PATH + FILE_PREFIX + fileName + ".json"
    if not os.path.exists(fullPath):
        print("Creating file: {}".format(fullPath))
        
    f = open(fullPath, mode=modes)
    for line in lines:
        print(line, file=f)
        
def storeValuesByKey(lines):
    print("Storing the sorted values to separate files")
    print("Files will be separated by a key value of '{}'".format(SORT_VALUE))
    for line in lines:
        #extract file name from value
        jsonObject = json.loads(line)
        value = jsonObject[SORT_VALUE]
        fileName = re.compile(REGEX).search(line).group()
        storeValues(fileName, [line], FILE_ACCESS_MODE) #without converting line to a list each character will be stored in its own line

downloaded = sorted(download(URL))
print("The downloaded and sorted values are:")
for item in downloaded:
    print(item)

storeValuesByKey(downloaded)

print("Finished")